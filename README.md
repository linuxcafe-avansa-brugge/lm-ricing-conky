# lm-ricing-conky

Het .conkyrc bestand gebruikt in de handleiding '**From Noob to Power User**' kan je installeren door de volgende handelingen te doen:


- Download het bestand via het git-commando
- Kopieer het bestandje in je home folder
- Start conky

Voer onderstaande command's uit:

- $ git clone https://gitlab.com/linuxcafe-avansa-brugge/lm-ricing-conky.git
- $ cd lm-ricing-conky.git
- $ cp .conkyrc ~/.
- $ conky

